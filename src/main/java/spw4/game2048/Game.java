package spw4.game2048;

import java.util.Random;
import java.util.Arrays;

/**
 * SPW4 Exercise 2
 * Authors: Georg Edlbauer, Andreas Pramendorfer
 */
public class Game {
    public static Random random = new Random();
    public static RandomTileGenerator tileGenerator = new RandomTileGenerator();
    public static final int BOARD_SIZE = 4;

    private int score = 0;
    private int moves = 0;
    private final int[][] board;

    public Game() {
        board = new int[BOARD_SIZE][BOARD_SIZE];
    }

    public int getScore() {
        return score;
    }

    public boolean isOver() {
        if (isWon())
            return true;

        for (int[] row : board) { // check if there are any empty tiles left
            for (int i : row) {
                if (i == 0)
                    return false;
            }
        }
        return true;
    }

    public boolean isWon() {
        return Arrays.stream(board).anyMatch(x ->
                Arrays.stream(x).anyMatch(y -> y >= 2048)
        );
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Moves: ").append(moves).append("\t");
        builder.append("Score: ").append(score).append('\n');
        for (int[] rows : board) {
            for (int i : rows) {
                builder.append(' ');
                if (i == 0) {
                    builder.append('.');
                } else {
                    builder.append(i);
                }
                builder.append(' ');
            }
            builder.append('\n');
        }
        return builder.toString();
    }

    public void initialize() {
        for (int i = 0; i < 2; i++) {
            spawnTile();
        }
    }

    public void move(Direction direction) {
        boolean hasMoved;
        boolean[][] mergeMatrix = new boolean[BOARD_SIZE][BOARD_SIZE];
        for (boolean[] row : mergeMatrix) {
            Arrays.fill(row, false);
        }

        do {
            hasMoved = false;
            switch (direction) {
                case left:
                    for (int i = 0; i < BOARD_SIZE; i++) {
                        for (int j = BOARD_SIZE - 1; j >= 0; j--) {
                            if (board[i][j] != 0 && j - 1 >= 0 && // if current tile is 0, or tile is at border --> no move
                                    ((!mergeMatrix[i][j] && !mergeMatrix[i][j - 1]) || board[i][j - 1] == 0) && // test if this tile was already merged this move
                                    (board[i][j - 1] == 0 || board[i][j] == board[i][j - 1])) { // test if destination is 0, or if merge is possible
                                if (board[i][j - 1] != 0) { // --> it's a merge
                                    mergeMatrix[i][j - 1] = true; // mark tile as merged this round
                                    score += board[i][j - 1] * 2;
                                } else {
                                    mergeMatrix[i][j - 1] = mergeMatrix[i][j]; // move merge info for tile to new tile position
                                    mergeMatrix[i][j] = false;
                                }
                                board[i][j - 1] += board[i][j]; // move tile
                                board[i][j] = 0;
                                hasMoved = true;
                            }
                        }
                    }
                    break;
                case up:
                    for (int i = BOARD_SIZE - 1; i >= 0; i--) {
                        for (int j = 0; j < BOARD_SIZE; j++) {
                            if (board[i][j] != 0 && i - 1 >= 0 && // if current tile is 0, or tile is at border --> no move
                                    ((!mergeMatrix[i][j] && !mergeMatrix[i - 1][j]) || board[i - 1][j] == 0) && // test if this tile was already merged this move
                                    (board[i - 1][j] == 0 || board[i][j] == board[i - 1][j])) { // test if destination is 0, or if merge is possible
                                if (board[i - 1][j] != 0) { // --> it's a merge
                                    mergeMatrix[i - 1][j] = true; // mark tile as merged this round
                                    score += board[i - 1][j] * 2;
                                } else {
                                    mergeMatrix[i - 1][j] = mergeMatrix[i][j]; // move merge info for tile to new tile position
                                    mergeMatrix[i][j] = false;
                                }
                                board[i - 1][j] += board[i][j]; // move tile
                                board[i][j] = 0;
                                hasMoved = true;
                            }
                        }
                    }
                    break;
                case down:
                    for (int i = 0; i < BOARD_SIZE; i++) {
                        for (int j = 0; j < BOARD_SIZE; j++) {
                            if (board[i][j] != 0 && i + 1 < BOARD_SIZE && // if current tile is 0, or tile is at border --> no move
                                    ((!mergeMatrix[i][j] && !mergeMatrix[i + 1][j]) || board[i + 1][j] == 0) && // test if this tile was already merged this move
                                    (board[i + 1][j] == 0 || board[i][j] == board[i + 1][j])) { // test if destination is 0, or if merge is possible
                                if (board[i + 1][j] != 0) { // --> it's a merge
                                    mergeMatrix[i + 1][j] = true; // mark tile as merged this round
                                    score += board[i + 1][j] * 2;
                                } else {
                                    mergeMatrix[i + 1][j] = mergeMatrix[i][j]; // move merge info for tile to new tile position
                                    mergeMatrix[i][j] = false;
                                }
                                board[i + 1][j] += board[i][j]; // move tile
                                board[i][j] = 0;
                                hasMoved = true;
                            }
                        }
                    }
                    break;
                case right:
                    for (int i = 0; i < BOARD_SIZE; i++) {
                        for (int j = 0; j < BOARD_SIZE; j++) {
                            if (board[i][j] != 0 && j + 1 < BOARD_SIZE && // if current tile is 0, or tile is at border --> no move
                                    ((!mergeMatrix[i][j] && !mergeMatrix[i][j + 1]) || board[i][j + 1] == 0) && // test if this tile was already merged this move
                                    (board[i][j + 1] == 0 || board[i][j] == board[i][j + 1])) { // test if destination is 0, or if merge is possible
                                if (board[i][j + 1] != 0) { // --> it's a merge
                                    mergeMatrix[i][j + 1] = true; // mark tile as merged this round
                                    score += board[i][j + 1] * 2;
                                } else {
                                    mergeMatrix[i][j + 1] = mergeMatrix[i][j]; // move merge info for tile to new tile position
                                    mergeMatrix[i][j] = false;
                                }
                                board[i][j + 1] += board[i][j]; // move tile
                                board[i][j] = 0;
                                hasMoved = true;
                            }
                        }
                    }
                    break;
            }
        } while (hasMoved); // if there was a move --> move again in case a "hole" was created
        spawnTile();
        ++moves;
    }

    private void spawnTile() {
        if (!isOver()) {
            int row, col;
            do {
                row = random.nextInt(BOARD_SIZE);
                col = random.nextInt(BOARD_SIZE);
            } while (board[row][col] != 0); // --> there is already a tile at the selected position --> retry
            board[row][col] = tileGenerator.getRandomTileNumber();
        }
    }

    public int[][] getBoard() {
        return board;
    }

    public int getMoves() {
        return moves;
    }

    public int getValueAt(int x, int y) {
        return board[x][y];
    }
}
