package spw4.game2048;

import java.util.Random;

/**
 * SPW4 Exercise 2
 * Authors: Georg Edlbauer, Andreas Pramendorfer
 */
public class RandomTileGenerator {
    public static Random random = new Random();

    public int getRandomTileNumber(){
        int randomInt = random.nextInt(10);
        if(randomInt == 9){
            return 4;
        }
        return 2;
    }
}
