package spw4.game2048;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * SPW4 Exercise 2
 * Authors: Georg Edlbauer, Andreas Pramendorfer
 */
@ExtendWith(MockitoExtension.class)
class GameTests {
    private Game game;

    @BeforeEach
    void beforeEach() {
        game = new Game();
    }

    @Test
    void testIsNotOver() {
        assertFalse(game.isOver());
    }

    @RepeatedTest(5)
    void testGameInitialization() {
        Game.random = new Random();
        RandomTileGenerator.random = new Random();
        // score must not be over 8 after init, since upper bounce is 4
        assertTrue(game.getScore() <= 8);
    }

    @Test
    void testRandomTileGenerator(@Mock Random rep) {
        when(rep.nextInt(anyInt()))
                .thenReturn(1)
                .thenReturn(5)
                .thenReturn(8)
                .thenReturn(9);

        RandomTileGenerator generator = new RandomTileGenerator();
        generator.random = rep;

        assertEquals(2, generator.getRandomTileNumber());
        assertEquals(2, generator.getRandomTileNumber());
        assertEquals(2, generator.getRandomTileNumber());
        assertEquals(4, generator.getRandomTileNumber());
    }

    @Test
    void testInitialize(@Mock Random rep, @Mock RandomTileGenerator repGenerator) {
        when(rep.nextInt(anyInt()))
                .thenReturn(0)
                .thenReturn(0)
                .thenReturn(2)
                .thenReturn(2);

        when(repGenerator.getRandomTileNumber())
                .thenReturn(2, 4);

        Game.random = rep;
        Game.tileGenerator = repGenerator;
        game.initialize();

        int [][] expectedBoard = {
                { 2, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 4, 0 },
                { 0, 0, 0, 0 }
        };

        assertArrayEquals(expectedBoard, game.getBoard());
    }

    @Test
    void testComplexGame(@Mock Random rep, @Mock RandomTileGenerator repGenerator) {
        when(rep.nextInt(anyInt()))
                .thenReturn(0).thenReturn(0)
                .thenReturn(0).thenReturn(1)
                .thenReturn(3).thenReturn(3)
                .thenReturn(3).thenReturn(2)
                .thenReturn(1).thenReturn(2)
                .thenReturn(3).thenReturn(0)
                .thenReturn(1).thenReturn(1);

        when(repGenerator.getRandomTileNumber())
                .thenReturn(2, 2, 4, 4, 4, 2, 2);

        Game.random = rep;
        Game.tileGenerator = repGenerator;
        game.initialize();

        game.move(Direction.left);
        game.move(Direction.left);
        game.move(Direction.up);
        game.move(Direction.up);
        game.move(Direction.right);

        int [][] expectedBoard = {
                { 0, 0, 0, 16 },
                { 0, 2, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 2 }
        };

        assertArrayEquals(expectedBoard, game.getBoard());
        assertEquals(36, game.getScore());
        assertFalse(game.isOver());
    }

    @Test
    void testIsWon(@Mock Random rep, @Mock RandomTileGenerator repGenerator) {
        when(rep.nextInt(anyInt()))
                .thenReturn(0).thenReturn(0)
                .thenReturn(0).thenReturn(1);

        when(repGenerator.getRandomTileNumber())
                .thenReturn(1024, 1024);

        Game.random = rep;
        Game.tileGenerator = repGenerator;
        game.initialize();

        assertFalse(game.isWon());
        game.move(Direction.right);
        // merging the two 1024 results in a 2048 -> we won the game
        assertTrue(game.isWon());
    }

    @Test
    void testIsOver() {
        game.getBoard()[0][1] = 32;
        game.getBoard()[0][2] = 32;

        assertFalse(game.isOver());

        for (int i = 0; i < Game.BOARD_SIZE; i++) {
            for (int j = 0; j < Game.BOARD_SIZE; j++) {
                game.getBoard()[i][j] = 2;
            }
        }

        // all positions are now set, game is over
        assertTrue(game.isOver());
    }

    @Test
    void testSingleMergePerMove(@Mock Random rep, @Mock RandomTileGenerator repGenerator){
        when(rep.nextInt(anyInt()))
                .thenReturn(0).thenReturn(3);

        when(repGenerator.getRandomTileNumber())
                .thenReturn(2);
        Game.tileGenerator = repGenerator;
        Game.random = rep;

        for (int i = 0; i < Game.BOARD_SIZE; i++) {
            game.getBoard()[0][i] = 2;
        }
        game.move(Direction.left);
        // one move must one merge a tile once
        int[][] expected = new int[][]{
                {4,4,0,2},
                {0,0,0,0},
                {0,0,0,0},
                {0,0,0,0},
        };
        assertArrayEquals(expected, game.getBoard());
    }

    @Test
    void testToString() {
        for (int i = 0; i < Game.BOARD_SIZE; i++) {
            for (int j = 0; j < Game.BOARD_SIZE; j++) {
                game.getBoard()[i][j] = i;
            }
        }

        assertEquals("Moves: 0\tScore: 0\n" +
                " .  .  .  . \n" +
                " 1  1  1  1 \n" +
                " 2  2  2  2 \n" +
                " 3  3  3  3 \n", game.toString());
    }

    @Test
    void testMergeDirection(@Mock Random rep, @Mock RandomTileGenerator repGenerator) {
        // test if neighboured tiles still merge if the space is packed
        when(rep.nextInt(anyInt()))
                .thenReturn(0).thenReturn(3);

        when(repGenerator.getRandomTileNumber())
                .thenReturn(2);
        Game.tileGenerator = repGenerator;
        Game.random = rep;

        game.getBoard()[0][0]=2;
        game.getBoard()[0][1]=2;
        game.getBoard()[0][2]=4;
        game.getBoard()[0][3]=4;

        game.move(Direction.left);
        // the 2s must merge and the 4s must merge
        int[][] expected = new int[][]{
                {4,8,0,2},
                {0,0,0,0},
                {0,0,0,0},
                {0,0,0,0},
        };
        assertArrayEquals(expected, game.getBoard());
    }

    @Test
    void testInvalidSpawnPosition(@Mock Random rep, @Mock RandomTileGenerator repGenerator) {
        // random returns the same position two times
        // Game implementation must try another time to get a valid position
        when(rep.nextInt(anyInt()))
                .thenReturn(0).thenReturn(3)
                .thenReturn(0).thenReturn(3)
                .thenReturn(0).thenReturn(2);

        when(repGenerator.getRandomTileNumber())
                .thenReturn(2, 2);

        Game.tileGenerator = repGenerator;
        Game.random = rep;
        game.initialize();

        int[][] expected = new int[][]{
                {0,0,2,2},
                {0,0,0,0},
                {0,0,0,0},
                {0,0,0,0},
        };
        assertArrayEquals(expected, game.getBoard());
    }

    /**
     * TestSimpleMoves was implemented first to test the basics of our implementation
     */
    @Nested
    class TestSimpleMoves {

        @BeforeEach
        void setupForMove(@Mock Random rep, @Mock RandomTileGenerator repGenerator) {
            // setting up tile values and position for each test
            when(rep.nextInt(anyInt()))
                    .thenReturn(0)
                    .thenReturn(0)
                    .thenReturn(2)
                    .thenReturn(2)
                    .thenReturn(1)
                    .thenReturn(0)
                    .thenReturn(0)
                    .thenReturn(3);

            when(repGenerator.getRandomTileNumber())
                    .thenReturn(2, 4, 2, 2);

            Game.random = rep;
            Game.tileGenerator = repGenerator;
            game.initialize();
        }

        @Test
        void testMoveLeft() {
            int [][] expectedBoard = {
                    { 2, 0, 0, 0 },
                    { 2, 0, 0, 0 },
                    { 4, 0, 0, 0 },
                    { 0, 0, 0, 0 }
            };
            game.move(Direction.left);
            assertArrayEquals(expectedBoard, game.getBoard());
        }

        @Test
        void testMoveRight() {
            int [][] expectedBoard = {
                    { 0, 0, 0, 2 },
                    { 2, 0, 0, 0 },
                    { 0, 0, 0, 4 },
                    { 0, 0, 0, 0 }
            };
            game.move(Direction.right);
            assertArrayEquals(expectedBoard, game.getBoard());
        }

        @Test
        void testMoveUp() {
            int [][] expectedBoard = {
                    { 2, 0, 4, 0 },
                    { 2, 0, 0, 0 },
                    { 0, 0, 0, 0 },
                    { 0, 0, 0, 0 }
            };
            game.move(Direction.up);
            assertArrayEquals(expectedBoard, game.getBoard());
        }

        @Test
        void testMoveDown() {
            int [][] expectedBoard = {
                    { 0, 0, 0, 0 },
                    { 2, 0, 0, 0 },
                    { 0, 0, 0, 0 },
                    { 2, 0, 4, 0 }
            };
            game.move(Direction.down);
            assertArrayEquals(expectedBoard, game.getBoard());
        }

        @Test
        void testMoveLeftThenDown() {
            int [][] expectedBoard = {
                    { 0, 0, 0, 2 },
                    { 0, 0, 0, 0 },
                    { 4, 0, 0, 0 }, // this 4 spawns at the end
                    { 4, 0, 0, 0 }
            };
            game.move(Direction.left);
            game.move(Direction.down);
            assertArrayEquals(expectedBoard, game.getBoard());
        }
    }
}
